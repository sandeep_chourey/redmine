# Redmine - project management software
# Copyright (C) 2006-2014  Jean-Philippe Lang
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

class Comment < ActiveRecord::Base
  include Redmine::SafeAttributes
  belongs_to :commented, :polymorphic => true, :counter_cache => true
  belongs_to :author, :class_name => 'User', :foreign_key => 'author_id'

  validates_presence_of :commented, :author, :comments

  after_create :send_notification

  safe_attributes 'comments'

  private

  def send_notification
    mailer_method = "#{commented.class.name.underscore}_comment_added"
    if Setting.notified_events.include?(mailer_method)
      comment_recipient_user_l=[]; comment_recipient_user_wl=[]; comment_cc_user_l=[]; comment_cc_user_wl=[];
      self.commented.recipients.each do |comment_recipient_user_mail|
        Mailer.check_is_local_email(comment_recipient_user_mail) ? comment_recipient_user_l.push(comment_recipient_user_mail) : comment_recipient_user_wl.push(comment_recipient_user_mail)
      end unless self.commented.recipients.empty?
      self.commented.watcher_recipients.each do |comment_cc_user_mail|
        Mailer.check_is_local_email(comment_cc_user_mail) ? comment_cc_user_l.push(comment_cc_user_mail) : comment_cc_user_wl.push(comment_cc_user_mail)
      end unless self.commented.watcher_recipients.empty?
      Mailer.send(mailer_method, self, comment_recipient_user_l, comment_cc_user_l).deliver
      Mailer.send(mailer_method, self, comment_recipient_user_wl, comment_cc_user_wl).deliver
    end
  end
end
