class CreateSystangoHrmSpecialRequests < ActiveRecord::Migration
  def change
    create_table :systango_hrm_special_requests do |t|
      t.column "request_type", :string
      t.column "leave_start_date", :date
      t.column "reason", :text
      t.references "user"
    end
  end
end
