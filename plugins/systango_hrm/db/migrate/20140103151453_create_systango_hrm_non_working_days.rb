class CreateSystangoHrmNonWorkingDays < ActiveRecord::Migration
  def change
    create_table :systango_hrm_non_working_days do |t|
      t.column "non_working_week_day", :string
      t.column "non_working_day", :string
      t.column "leave_consider", :string
      t.column "non_working_week_days", :string
      t.column "non_working_day_year", :integer
    end
  end
end
