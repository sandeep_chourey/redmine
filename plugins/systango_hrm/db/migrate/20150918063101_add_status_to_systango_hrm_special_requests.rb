class AddStatusToSystangoHrmSpecialRequests < ActiveRecord::Migration
  def change
    add_column :systango_hrm_special_requests, :status, :string
  end
end
