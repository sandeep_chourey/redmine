module SystangoHrm
  module WelcomeHelperPatch
    def self.included(base)
       base.send(:include, InstanceMethods)
       base.class_eval do
         unloadable
       end
    end
    module InstanceMethods
      #TODO Are we using this Custom Field?
      def customfield
        CustomField.where(:name=> "Employee Code")
      end
      
      def leaves_of_current_user
        SystangoHrmEmployeeLeave.by_user_id_or_referral_id(User.current.id).order_by_created_desc
      end

      def pending_hr_leave
        admin_request = SystangoHrmRequestReceiver.where('receiver_approval_status = ? AND receiver_id != ? AND application_id IS NOT NULL','approved',User.current.id) if User.current.hr_user?
        if admin_request.present? && User.current.hr_user?
          pending_leave = SystangoHrmRequestReceiver.final_approver_pending_leave.where(application_id: admin_request.pluck(:application_id), receiver_id: User.current.id).order('id DESC')
        end
      end
      
      def pending_employee_leaves
        #(SystangoHrmRequestReceiver.request_by_user_and_type(User.current, SystangoHrm::Constants::STATUS_PENDING).uniq.compact rescue [])
        all_user_leaves = []
        subordinates = User.current.teamleads
        if subordinates.count > 0
          subordinates.each do |subordinate_user|
            leaves = SystangoHrmEmployeeLeave.find_all_by_user_id_and_status(subordinate_user.subordinate_user_id, 'pending').reverse
            all_user_leaves << leaves
          end
        end
        all_user_leaves.flatten!
      end

      def pending_special_request_employee_leaves
        #(SystangoHrmRequestReceiver.request_by_user_and_type(User.current, SystangoHrm::Constants::STATUS_PENDING).uniq.compact rescue [])
        all_user_leaves = []
        subordinates = User.current.teamleads
        if subordinates.count > 0
          subordinates.each do |subordinate_user|
            leaves = SystangoHrmSpecialRequest.find_all_by_user_id_and_status(subordinate_user.subordinate_user_id, 'pending').reverse
            all_user_leaves << leaves
          end
        end
        all_user_leaves.flatten!
      end

      def pending_special_request_final_approver
        #(SystangoHrmRequestReceiver.request_by_user_and_type(User.current, SystangoHrm::Constants::STATUS_PENDING).uniq.compact rescue [])
        admin_request = SystangoHrmRequestReceiver.where('receiver_approval_status = ? AND receiver_id != ? AND special_request_application_id IS NOT NULL','approved',User.current.id) if User.current.hr_user?
        # if admin_request.present? && User.current.hr_user?
        pending_hr_leave = SystangoHrmRequestReceiver.final_approver_pending_leave.where(special_request_application_id: admin_request.pluck(:special_request_application_id), receiver_id: User.current.id ).order('id DESC')
        # end
      end

      def pending_compoff_request_employee_leaves
        #(SystangoHrmRequestReceiver.request_by_user_and_type(User.current, SystangoHrm::Constants::STATUS_PENDING).uniq.compact rescue [])
        all_user_leaves = []
        subordinates = User.current.teamleads
        if subordinates.count > 0
          subordinates.each do |subordinate_user|
            leaves = SystangoHrmCompoff.find_all_by_user_id_and_status(subordinate_user.subordinate_user_id, 'pending').reverse
            all_user_leaves << leaves
          end
        end
        all_user_leaves.flatten!
      end

      def special_request_employee_leaves
        # (SystangoHrmSpecialRequest.find_all_by_user_id(User.current.id))
        #if User.current.admin_user? or User.current.allowed_to_globally?(:manage_leave_request, {})
        if User.current.hr_user?
          special_request_admin_request = SystangoHrmRequestReceiver.where('receiver_approval_status = ? AND receiver_id != ? AND special_request_application_id IS NOT NULL','approved',User.current.id) if User.current.hr_user?
          hr_pending_leave = SystangoHrmRequestReceiver.final_approver_pending_leave.where(special_request_application_id: special_request_admin_request.pluck(:special_request_application_id), receiver_id: User.current.id ).order('id DESC')
        elsif User.current.allowed_to_globally?(:manage_leave_request, {})
          special_request_admin_request = SystangoHrmRequestReceiver.where('receiver_approval_status = ? AND receiver_id != ? AND special_request_application_id IS NOT NULL','pending',User.current.id).order('id DESC')
        elsif User.current.admin_user? or User.current.allowed_to_globally?(:manage_leave_request, {})
          special_request_admin_request = SystangoHrmRequestReceiver.where('receiver_approval_status = ? AND receiver_id != ? AND special_request_application_id IS NOT NULL','pending',User.current.id).order('id DESC')
        else
          special_request_admin_request = SystangoHrmSpecialRequest.find_all_by_user_id_and_status(User.current.id, 'pending').reverse
        end
      end

      def special_request_latest_leaves
        # (SystangoHrmSpecialRequest.find_all_by_user_id(User.current.id))
        #if User.current.admin_user? or User.current.allowed_to_globally?(:manage_leave_request, {})
        if User.current.allowed_to_globally?(:manage_leave_request, {})
          special_request_admin_request = SystangoHrmSpecialRequest.find_all_by_user_id_and_status(User.current.id, 'pending').reverse
        end
      end

      def special_request_latest_final_approver_leaves
        # (SystangoHrmSpecialRequest.find_all_by_user_id(User.current.id))
        #if User.current.admin_user? or User.current.allowed_to_globally?(:manage_leave_request, {})
        if User.current.hr_user?
          special_request_admin_request = SystangoHrmSpecialRequest.find_all_by_user_id_and_status(User.current.id, 'pending').reverse
        end
      end

      def comp_off_request_employee_leaves
        if User.current.allowed_to_globally?(:manage_leave_request, {})
          comp_request_user_request = SystangoHrmCompoff.find_all_by_user_id_and_status(User.current.id, 'pending').reverse
          #comp_request_admin_request = SystangoHrmRequestReceiver.where('receiver_approval_status = ? AND receiver_id = ? AND comp_off_application_id IS NOT NULL','pending',User.current.id) if User.current.allowed_to_globally?(:manage_leave_request, {})
         else
          comp_request_user_request = SystangoHrmCompoff.find_all_by_user_id_and_status(User.current.id, 'pending').reverse
        end
      end

      def custom_value?
        CustomValue.all.blank?
      end

      def link_to_go_back
        link_to_function(l(:go_back), "history.back()")
      end

      def link_to_leave_dash_board
        link_to l(:label_leave_dashboard), systango_hrm_employee_leaves_path
      end

      def leave_date_format(leave, date_type)
        date_format = (leave.is_half_day ? "%b %d, %y %H:%M" : "%b %d, %y")
        leave.send("leave_#{date_type}").strftime(date_format)
      end

      def apply_type(leave)
        leave.referral_id.nil? ? l(:label_self) : (leave.user ? leave.user.name : l(:label_refer))
      end

      def user_permission?(permission_for)
        (User.current.allowed_to_globally?(:"#{permission_for}", {}) or User.current.allowed_to_globally?(:hr_permissions, {}))
      end

      def user_has_any_systango_hrm_permission?
        ['apply_leave', 'view_leave_report_self', 'manage_leave_request', 'view_leave_report_hr_or_tl','add_compoff_details',
         'show_subordinate_and_its_teamlead', 'add_teamlead_subordinates_details', 'add_designation_wise_entitled_leaves',
         'add_employee_leave_details', 'add_subject_for_leave', 'add_holiday_calendar', 'hr_permissions'].each do |permission_for|
          return true if user_permission?(permission_for)
        end
        false
      end

    end
  end
end