module SystangoHrmAttendancesHelper
  def validate_leave?(employee_leaves, leave_days, day)
    employee_leave_subject = nil
    employee_leaves.map{|employee_leave| employee_leave_subject = get_leave_subject(employee_leave,leave_days,day) if employee_leave.include_days?(day.split('/').second.to_i) }
    employee_leave_subject
  end

  def get_leave_subject(employee_leave,leave_days,day)
    if (employee_leave.leave_start_date.to_s(:db).to_datetime.day == day.split('/').second.to_i && employee_leave.is_start_date_enabled) || (employee_leave.leave_end_date.to_s(:db).to_datetime.day == day.split('/').second.to_i && employee_leave.is_end_date_enabled)
      employee_leave.systango_hrm_subject.subject
    elsif leave_days.include?(day.split('/').second.to_i)
      employee_leave.systango_hrm_subject.subject
    end
  end

  def get_employee_leaves(employee_leaves)
    employee_leaves.map{|p| (p.leave_start_date.day..p.leave_end_date.day).to_a }.flatten
  end

  def get_holidays
    SystangoHrmHoliday.all.map{|holiday| holiday.holiday_date.strftime("%m/%d/%Y")}
  end

  def get_attendances(attendances, day, user_id)
    attendances.select{|a| a.day_in.present? && a.day_in.strftime("%m/%d/%Y") == day && a.user_id == user_id}.first
  end

  def check_for_leave_types?(leave_days, day, employee_leaves, type)
    leave_days.include?(day.split('/').second.to_i) && employee_leaves.select{|employee_leave| employee_leave.include_days?(day.split('/').second.to_i) && employee_leave.status.downcase == type}.size > 0
  end

  def get_non_working_days(year, month)
    non_working_days = SystangoHrmNonWorkingDay.where(non_working_day_year: year)
    results = []
    non_working_days.each do |nwd|
       date = Date.new year.to_i, month.to_i, 1
       day = Time::DAYS_INTO_WEEK[:"#{nwd.non_working_week_day.downcase}"]
       if nwd.non_working_week_days == 'Even week days'
         results << [2 + ((day-date.wday) % 7) + 7, 2 + ((day-date.wday) % 7) + 21]
       elsif nwd.non_working_week_days == 'Odd week days'
         results << [2 + ((day-date.wday) % 7) ,2 + ((day-date.wday) % 7) + 14, 2 + ((day-date.wday) % 7) + 28]
       else
         results << [2 + ((day-date.wday) % 7) ,2 + ((day-date.wday) % 7) + 14, 2 + ((day-date.wday) % 7) + 28,2 + ((day-date.wday) % 7) + 7, 2 + ((day-date.wday) % 7) + 21]
       end
    end
    results.flatten.sort!
  end

  def get_dates even_or_odd, day

  end

end
