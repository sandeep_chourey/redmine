class SystangoHrmSpecialRequestsController < ApplicationController
  unloadable
  before_filter :initialize_admin_approved_leave, only: :index
  before_filter :authorize_to_view_application, only: [:application,:update_leave_status]
  before_filter :leave_application_details, only: [:new, :create, :edit, :update, :application, :accepted, :unapproved, :pending, :manage_request, :update_leave_status, :get_leave_balance]
  before_filter :leave_application_instance, only: [:new, :create, :manage_request, :edit, :update, :application]

  def index
    if User.current.admin? || User.current.allowed_to_globally?(:manage_leave_request, {}) || User.current.hr_user?
      @special_requests = params[:status].present? ? SystangoHrmSpecialRequest.find_all_by_status(params[:status]) : SystangoHrmSpecialRequest.find_all_by_status('pending')
    else
      @special_requests = []
      @special_requests << (params[:status].present? ? SystangoHrmSpecialRequest.find_all_by_user_id_and_status(User.current.id, params[:status]) : SystangoHrmSpecialRequest.find_by_user_id_and_status(User.current.id, 'pending'))
      @special_requests.flatten!
    end
    @teamlead_users = SystangoHrmTeamleadsSubordinates.unlocked_teamleads_for(User.current.subordinates)
    @default_application_receivers = ((@teamlead_users.blank? ? [] : @teamlead_users)).uniq
    @users_with_hr_permission = User.users_with_hr_permission.reject(&:blank?)
    if params[:status].present?
      respond_to do |format|
        format.js
      end
    end
  end

  def new
    @special_request = SystangoHrmSpecialRequest.new
    @teamlead_users = SystangoHrmTeamleadsSubordinates.unlocked_teamleads_for(User.current.subordinates)
    @default_application_receivers = ((@teamlead_users.blank? ? [] : @teamlead_users)).uniq
    @users_with_hr_permission = User.users_with_hr_permission.reject(&:blank?)
    # @non_working_days = SystangoHrmHoliday.all.map{|holiday| holiday.holiday_date.strftime("%Y-%m-%d")}
  end

  def create
    @special_request = SystangoHrmSpecialRequest.new(params[:special_request])
    if (params[:special_request][:'hours_minutes(4i)'] == "" || params[:special_request][:'hours_minutes(5i)'] == "")
      @special_request.hours_minutes = nil
    end
    if @special_request.valid? && @special_request.errors.empty?
      @special_request.save
      @special_request.systango_hrm_request_receivers.create(:receiver_id => params[:first_approver].to_i)
      @special_request.systango_hrm_request_receivers.create(:receiver_id => params[:final_approver].to_i)
      SystangoHrmMailer.delay.notify_first_approval_for_special_leave(params[:first_approver], User.current, @special_request)
      SystangoHrmMailer.delay.notify_employee_of_their_special_leave(User.current, @special_request)
      flash[:notice] = l(:special_request_applied)
      redirect_to systango_hrm_employee_leaves_path
    else
      render :action => 'new'
    end
  end

  def update_special_leave_status
    employee_leave = SystangoHrmSpecialRequest.find(params[:id])
    #TODO :: Why is this variable named as request_changed_by_user ?? What does it signify? [Palash] : Its show which user is changing status of leave application.
    @request_changed_by_user = SystangoHrmRequestReceiver.by_special_application_and_receiver_id(params[:id], User.current.id).first
    unless params[:cancel]
      unapprove_leave = (params[:unapprove] == SystangoHrm::Constants::STATUS_UNAPPROVED)
      @request_changed_by_user.update_special_leave_status(unapprove_leave, params[:systango_hrm_special_requests][:comment])
      # render 'application' and return if @request_changed_by_user.errors.any?
      #render 'application'
    end
    @user_subordinates = SystangoHrmSpecialRequest.find(params[:id]).user.subordinates
    @teamlead_users = SystangoHrmTeamleadsSubordinates.unlocked_teamleads_for(@user_subordinates)
    @first_approver = (((@teamlead_users.blank? ? [] : @teamlead_users)).uniq).first
    @final_approver = User.users_with_hr_permission.reject(&:blank?).last
    # @manager_approver = User.users_with_manager_permission.reject(&:blank?).first
    if params[:approve]
      if params[:systango_hrm_special_requests][:comment].blank?
        flash[:warning] = l(:comment_blank_data_error)
        redirect_to application_systango_hrm_special_request_path(employee_leave) and return
      end
      if User.current.id == @first_approver.id
        employee_leave.status = 'first_approved'
        SystangoHrmMailer.delay.notify_final_approval_for_employee_special_leave(@final_approver.id, employee_leave)
        SystangoHrmMailer.delay.notify_user_for_special_leave(employee_leave.user_id, employee_leave)
      elsif User.current.id == @final_approver.id
        employee_leave.status = 'approved'
        employee_leave.save
        SystangoHrmMailer.delay.notify_user_for_approved_employee_special_leave(employee_leave.user_id, employee_leave)
      end
    elsif params[:cancel]
          receivers = @teamlead_users.first.mail.to_a
          employee_leave.cancel_status
          employee_leave.save
          SystangoHrmMailer.delay.notify_all_of_special_leave_cancelled(employee_leave, User.current, receivers)
    else
      if params[:systango_hrm_special_requests][:comment].blank?
        flash[:warning] = l(:comment_blank_data_error)
        redirect_to application_systango_hrm_special_request_path(employee_leave) and return
      end
      employee_leave.status = 'unapproved'
      first_approver_comment = params[:systango_hrm_special_requests][:comment]
      employee_leave.save
      SystangoHrmMailer.delay.notify_user_for_unapproved_special_leave(employee_leave.user_id, employee_leave, first_approver_comment)
    end
    flash[:notice] = l(:leave_status_change_notice)
    redirect_to application_systango_hrm_special_request_path(employee_leave)
  end

  ["approved" , "unapproved","pending"].each do |status|
    define_method status do
     @leave_status = status
     if params[:category] =='special_request' && params[:action] == 'pending' && User.current.allowed_to_globally?(:manage_leave_request, {})
       @all_leaves = SystangoHrmRequestReceiver.request_by_user_and_type_for_special_leave(User.current, 'pending').reverse.compact.paginate(:page => params[:page], :per_page => 10) rescue nil
     elsif params[:category] =='special_request' && params[:action] == 'pending' &&  User.current.admin_user? && User.current.present?
       hr_leaves = SystangoHrmRequestReceiver.where('receiver_approval_status = ? AND receiver_id != ? AND special_request_application_id IS NOT NULL','approved',User.current.id).compact
       @all_leaves = SystangoHrmRequestReceiver.final_approver_pending_leave.where(special_request_application_id: hr_leaves.collect(&:special_request_application_id), receiver_id: User.current.id).reverse.compact.paginate(:page => params[:page], :per_page => 10) rescue nil
     elsif params[:category] =='special_request' && params[:action] == 'pending' &&  !User.current.admin_user? && !User.current.allowed_to_globally?(:manage_leave_request, {})
       @all_leaves = SystangoHrmSpecialRequest.where('user_id = ? AND status= ?',User.current.id,'pending').reverse.compact.paginate(:page => params[:page], :per_page => 10) rescue nil
     elsif params[:category] =='pending_special_request' && params[:action] == 'pending'
       @special_request_pending_leaves = SystangoHrmSpecialRequest.where('user_id != ? AND status= ?',User.current.id,'pending').reverse.compact.paginate(:page => params[:page], :per_page => 10) rescue nil
     elsif params[:category]=='latest_special_request' && params[:action] == 'pending' && User.current.allowed_to_globally?(:manage_leave_request, {})
       @manager_latest_leaves = SystangoHrmSpecialRequest.where('user_id = ? AND status= ?',User.current.id,'pending').reverse.compact.paginate(:page => params[:page], :per_page => 10) rescue nil
     elsif params[:category]== 'latest_special_request_final_approver' && params[:action] == 'pending' && User.current.hr_user?
       @final_approver_latest_leaves = SystangoHrmSpecialRequest.where('user_id = ? AND status= ? ',User.current.id,'pending').reverse.compact.paginate(:page => params[:page], :per_page => 10) rescue nil
     elsif params[:category]== 'hr_pending_special_request' && params[:action] == 'pending' && User.current.hr_user?
       admin_request = SystangoHrmRequestReceiver.where('receiver_approval_status = ? AND receiver_id != ? AND special_request_application_id IS NOT NULL','approved',User.current.id) if User.current.hr_user?
       @final_approver_pending_leaves = SystangoHrmRequestReceiver.final_approver_pending_leave.where(special_request_application_id: admin_request.pluck(:special_request_application_id), receiver_id: User.current.id ).reverse.compact.paginate(:page => params[:page], :per_page => 10) rescue nil
     else
       @all_leaves = SystangoHrmRequestReceiver.request_by_user_and_type_for_special_leave(User.current, @leave_status).reverse.compact.paginate(:page => params[:page], :per_page => 10) rescue nil
     end
      render 'more_leaves'
    end
  end

  def application
    if params[:id].present?
      @leave_status = SystangoHrmRequestReceiver.where(receiver_approval_status: ['approved','unapproved'], special_request_application_id: params[:id])
    end
    @final_approver_request = @leave_detail.systango_hrm_request_receivers.find_by_receiver_id(@users_with_hr_permission)
  end

  def destroy
    @special_request = SystangoHrmSpecialRequest.find(params[:id])
    @special_request.destroy
    flash[:notice] = l(:special_request_delete_notice)
    redirect_to systango_hrm_special_requests_path
  end

  private

  def initialize_admin_approved_leave
    admin_request = SystangoHrmRequestReceiver.where('receiver_approval_status = ? AND receiver_id != ?','approved',User.current.id) if User.current.hr_user?
    if admin_request.present? && User.current.hr_user?
      @pending_leave = SystangoHrmRequestReceiver.final_approver_pending_leave.where(special_request_application_id: admin_request.pluck(:special_request_application_id), receiver_id: User.current.id )
    end
  end

  def authorize_to_view_application
    @leave_detail = SystangoHrmSpecialRequest.find(params[:id]) rescue nil
    # if @leave_detail.blank? or (SystangoHrmRequestReceiver.by_application_and_receiver_id(params[:id], User.current.id).blank? and !(User.current.id == (@leave_detail.applied_user.id)))
    if @leave_detail.blank?
      flash[:error] = l(:application_view_authorisation_error)
      redirect_to home_path
    end
  end

  def leave_application_details
    leave_application_instance
    user_ids = (@default_application_receivers + @users_with_hr_permission.to_a).map{|user| user.id}
    user_ids.each do |user|
      @request_recievers_for_leave = SystangoHrmRequestReceiver.by_application_and_receiver_id(params[:id].to_i, user)
    end
    @request_reciever = SystangoHrmRequestReceiver.by_special_application_and_receiver_id(params[:id].to_i, User.current.id).first
  end

  def leave_application_instance
    #admins = User.admin_users
    #active_and_admin_users = User.active_users - admins - [User.current]
    active_and_admin_users = User.active_users - [User.current]
    active_and_admin_users = active_and_admin_users - @teamlead_users if @teamlead_users
    @teamlead_details = active_and_admin_users.sort_by{|e| e.firstname.titleize}
    #@default_application_receivers = (admins + (@teamlead_users.blank? ? [] : @teamlead_users)).uniq
    @default_application_receivers = ((@teamlead_users.blank? ? [] : @teamlead_users)).uniq
    hr_permission_users = User.users_with_hr_permission.reject(&:blank?)
    if hr_permission_users.present?
      @users_with_hr_permission = hr_permission_users
    end
    @default_application_receivers = @default_application_receivers - [User.current] if (!@users_with_hr_permission.blank? and @users_with_hr_permission.include?(User.current.id))
    @default_application_receivers = @default_application_receivers.reject(&:blank?)
    subordinates_with_leave_account = (User.current.teamleads.collect{|teamlead| teamlead if teamlead.subordinate_user.systango_hrm_leave_account}).compact
    @subordinates_with_leave_account = SystangoHrmTeamleadsSubordinates.unlocked_subordinates_for(subordinates_with_leave_account)
    #@subjects = SystangoHrmSubject.all_as_array
    @subjects = SystangoHrmSubject.all(conditions: ['is_deleted', false])
  end

end
