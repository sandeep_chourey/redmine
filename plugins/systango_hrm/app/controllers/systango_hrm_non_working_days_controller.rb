class SystangoHrmNonWorkingDaysController < ApplicationController
  unloadable

  def new
    @non_working_day = SystangoHrmNonWorkingDay.new
  end

  def create
    @non_working_day = SystangoHrmNonWorkingDay.count(:conditions => ["non_working_week_day = ? and non_working_day = ? and leave_consider = ? and non_working_week_days = ? and non_working_day_year = ?", params[:non_working_day][:non_working_week_day], params[:non_working_day][:non_working_day], params[:non_working_day][:leave_consider], params[:non_working_day][:non_working_week_days], params[:non_working_day_year]])
    if @non_working_day == 0
      @non_working_day = SystangoHrmNonWorkingDay.new(params[:non_working_day]) do |nwd|
        nwd.non_working_day_year = params[:non_working_day_year]
      end
      @non_working_day.save
      @non_working_days = SystangoHrmNonWorkingDay.find(:all, :conditions => ["non_working_day_year = ?", params[:non_working_day_year]], :order => "id ASC")
      flash[:notice] = l(:non_working_day_add_notice)
      respond_to do |format|
        format.js
      end
    else
      flash[:warning] = l(:non_working_day_exist_notice)
    end
  end

  def edit
    @non_working_day = SystangoHrmNonWorkingDay.find(params[:id])
    #redirect_to systango_hrm_holidays_path if @non_working_day.non_working_day_year <= Date.today.year
  end

  def update
    @non_working_day = SystangoHrmNonWorkingDay.find(params[:id])
    @non_working_day.attributes = params[:non_working_day]
    @non_working_day.non_working_day_year = params[:non_working_day_year]
    if @non_working_day.save
      flash[:notice] = l(:non_working_day_update_notice)
      redirect_to systango_hrm_holidays_path
    else
      render 'edit'
    end
  end

  def destroy
    @non_working_day = SystangoHrmNonWorkingDay.find(params[:id])
    @year = @non_working_day.non_working_day_year
    @non_working_day.destroy
    flash[:notice] = l(:non_working_day_delete_notice)
    redirect_to systango_hrm_holidays_path
  end

end
