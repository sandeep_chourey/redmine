class SystangoHrmAttendancesController < ApplicationController
  unloadable
  def index
    @month = params[:month] || Date.today.month
    @year = params[:year] || Date.today.year
    if User.current.admin?
      @selected_users = User.order('firstname ASC, lastname ASC').all
    elsif User.current.teamleads.map(&:teamlead_user_id).include?(User.current.id)
      @subordinate_users = SystangoHrmTeamleadsSubordinates.unlocked_subordinates_for(User.current.teamleads)
      @selected_users = (@subordinate_users << User.current)
      @selected_users = @selected_users.sort_by{|user|[user.firstname, user.lastname]}
    else
      @selected_users = [User.current]
    end
    if params[:user_id].present?
      @users = @selected_users.collect{|user| user if user.id == params[:user_id].to_i}.compact
    else
      @users = @selected_users
    end
    #@users_leaves = SystangoHrmEmployeeLeave.where('users.id IN (?)',@users AND ("(MONTH(systango_hrm_employee_leaves.leave_start_date) = ? OR MONTH(systango_hrm_employee_leaves.leave_end_date) = ? ) AND YEAR(systango_hrm_employee_leaves.leave_start_date) = ?", '05', '01', '2015')
    @users_leaves = SystangoHrmEmployeeLeave.where('user_id IN (?) AND (MONTH(systango_hrm_employee_leaves.leave_start_date) = ? OR MONTH(systango_hrm_employee_leaves.leave_end_date) = ? ) AND YEAR(systango_hrm_employee_leaves.leave_start_date) = ?', @users, @month, @month, @year)
    @attendances = SystangoHrmAttendance.includes(:user).where('user_id IN (?) AND (MONTH(employee_attendance.day_in)) = ? AND (YEAR(employee_attendance.day_in)) = ?', @users, @month, @year)

    @start_d = Time.utc(@year, @month).beginning_of_month
    @end_d = Time.utc(@year, @month).end_of_month
    @number_of_days = (@start_d.to_date..@end_d.to_date).map{ |date| [date.strftime("%m/%d/%Y"), date.wday]}
    @wday = @number_of_days.transpose.last
    if params[:year].present? && params[:month].present?
      respond_to do |format|
        format.js
      end
    end
  end

  def attendance_detail
    if params[:user_id].present?
      @user = User.find(params[:user_id])
      @day = DateTime.strptime(params[:day], "%m/%d/%Y")
      @punch_date = Punchdata.where('enroll_no= ? AND punch_date =? ',@user.enroll_number, @day.strftime("%Y/%m/%d"))
      #@punch_date = Punchdata.where('enroll_no= ? AND punch_date =? ', 18364821,"2013/05/30")
      render :layout => "fancybox"
  end
  end

end
