class SystangoHrmEmployeeLeavesController < SystangoHrmController
  include SystangoHrmAttendancesHelper
  unloadable
  before_filter :find_or_initialize_employee_leave
  # Don't change order of filters. it is chained.
  before_filter :check_leave_account_is_created, :fetch_teamleads_subordinates, only: [:new, :create, :edit, :update, :application]
  before_filter :leave_application_details, only: [:new, :create, :edit, :update, :application, :accepted, :unapproved, :pending, :manage_request, :update_leave_status, :get_leave_balance]
  before_filter :leave_application_instance, only: [:new, :create, :manage_request, :edit, :update, :application]
  before_filter :authorize_to_view_application, only: [:application,:update_leave_status]
  before_filter :initialize_employee_leave, only: :update
  before_filter :initialize_admin_approved_leave, only: :index
  # before_filter :initialize_admin_approved_special_leave, only: :index

  #before_filter :validate_leave_type, only: [:create, :update]

  def all_leave
    if User.current.admin?
      @leave_requests = SystangoHrmEmployeeLeave.order("created_at DESC")
    end
  end

  def new
	  @employee_leave = SystangoHrmEmployeeLeave.new
    # @non_working_days = SystangoHrmHoliday.all.map{|holiday| holiday.holiday_date.strftime("%Y-%m-%d")}
	  render :template => "systango_hrm_employee_leaves/create_update_leaves"
	end

  def create
    @employee_leave = SystangoHrmEmployeeLeave.new(params[:systango_hrm_employee_leave].merge!(user_id: User.current.id))
    if @employee_leave.valid?

      start_date_year, start_date_month = params[:systango_hrm_employee_leave][:leave_start_date].to_date.strftime("%Y"), params[:systango_hrm_employee_leave][:leave_start_date].to_date.strftime("%m")
      end_date_year, end_date_month = params[:systango_hrm_employee_leave][:leave_end_date].to_date.strftime("%Y"), params[:systango_hrm_employee_leave][:leave_end_date].to_date.strftime("%m")

      if start_date_year == end_date_year
        if start_date_month == end_date_month
          non_working_days = get_non_working_days(start_date_year, start_date_month)
          nwdays = non_working_days.collect{|d| "#{start_date_year}-#{start_date_month}-#{d}".to_date.strftime("%Y-%m-%d") rescue ''}.compact.reject { |c| c.blank? }
        else
          non_working_days1 = get_non_working_days(start_date_year, start_date_month)
          nwdays1 = non_working_days1.collect{|d| "#{start_date_year}-#{start_date_month}-#{d}".to_date.strftime("%Y-%m-%d") rescue ''}.compact.reject { |c| c.blank? }
          non_working_days2 = get_non_working_days(end_date_year, end_date_month)
          nwdays2 = non_working_days2.collect{|d| "#{end_date_year}-#{end_date_month}-#{d}".to_date.strftime("%Y-%m-%d") rescue ''}.compact.reject { |c| c.blank? }
          nwdays = nwdays1.concat(nwdays2)
        end
      else
        non_working_days1 = get_non_working_days(start_date_year, start_date_month)
        nwdays1 = non_working_days1.collect{|d| "#{start_date_year}-#{start_date_month}-#{d}".to_date.strftime("%Y-%m-%d") rescue ''}.compact.reject { |c| c.blank? }
        non_working_days2 = get_non_working_days(end_date_year, end_date_month)
        nwdays2 = non_working_days2.collect{|d| "#{end_date_year}-#{end_date_month}-#{d}".to_date.strftime("%Y-%m-%d") rescue ''}.compact.reject { |c| c.blank? }
        nwdays = nwdays1.concat(nwdays2)
      end

      holidays = SystangoHrmHoliday.all.map{|holiday| holiday.holiday_date.strftime("%Y-%m-%d")}
      applied_leave_date_ranges = (params[:systango_hrm_employee_leave][:leave_start_date]..params[:systango_hrm_employee_leave][:leave_end_date]).to_a
      # if applied_leave_date_ranges.any? {|applied_leave_date| holidays.include?(applied_leave_date)}
      if !(applied_leave_date_ranges & (holidays.concat(nwdays)).uniq).empty?
        flash[:warning] = "Leave/Leaves can't be taken on Holidays or Non Working Days"
        redirect_to_corresponding_path
      else
        if (params[:systango_hrm_employee_leave][:start_date_first_half].present? || params[:systango_hrm_employee_leave][:start_date_second_half].present?) && (params[:systango_hrm_employee_leave][:leave_date_first_half].present? || params[:systango_hrm_employee_leave][:leave_date_second_half].present?)
          conditions = ["DATE_FORMAT(leave_start_date, '%Y-%m-%d') = ? AND DATE_FORMAT(leave_end_date, '%Y-%m-%d') = ? AND user_id = ? AND status = ? AND is_start_date_enabled = ? AND is_end_date_enabled = ?", params[:systango_hrm_employee_leave][:leave_start_date], params[:systango_hrm_employee_leave][:leave_end_date], User.current.id, 'pending', true, true]
        elsif (params[:systango_hrm_employee_leave][:start_date_first_half].present? || params[:systango_hrm_employee_leave][:start_date_second_half].present?)
          conditions = ["DATE_FORMAT(leave_start_date, '%Y-%m-%d') = ? AND DATE_FORMAT(leave_end_date, '%Y-%m-%d') = ? AND user_id = ? AND status = ? AND is_start_date_enabled = ?", params[:systango_hrm_employee_leave][:leave_start_date], params[:systango_hrm_employee_leave][:leave_end_date], User.current.id, 'pending', true]
        elsif (params[:systango_hrm_employee_leave][:leave_date_first_half].present? || params[:systango_hrm_employee_leave][:leave_date_second_half].present?)
          conditions = ["DATE_FORMAT(leave_start_date, '%Y-%m-%d') = ? AND DATE_FORMAT(leave_end_date, '%Y-%m-%d') = ? AND user_id = ? AND status = ? AND is_end_date_enabled = ?", params[:systango_hrm_employee_leave][:leave_start_date], params[:systango_hrm_employee_leave][:leave_end_date], User.current.id, 'pending', true]
        else
          conditions = ["DATE_FORMAT(leave_start_date, '%Y-%m-%d') = ? AND DATE_FORMAT(leave_end_date, '%Y-%m-%d') = ? AND user_id = ? AND status = ?", params[:systango_hrm_employee_leave][:leave_start_date], params[:systango_hrm_employee_leave][:leave_end_date], User.current.id, 'pending']
        end
        @employee_leaves_count = SystangoHrmEmployeeLeave.count(:conditions => conditions)
        if @employee_leaves_count == 0
          @employee_leave = SystangoHrmEmployeeLeave.new(params[:systango_hrm_employee_leave].merge!(user_id: User.current.id))
          #@employee_leave.referral_id = params[:systango_hrm_employee_leave][:referral_id] if (params[:apply][:leave] == (SystangoHrm::Constants::REFER))
          if @employee_leave.valid?
            #@teamlead_users = SystangoHrmTeamleadsSubordinates.unlocked_teamleads_for(SystangoHrmTeamleadsSubordinates.by_subordinate_user_id(params[:systango_hrm_employee_leave][:referral_id]).to_a) if params[:apply][:leave] == (SystangoHrm::Constants::REFER)
            # calling private method for fetching admin, hr and teamlead ids
            # tl_admin_hr_ids.each {|tl_admin_hr_id| @employee_leave.systango_hrm_request_receivers << SystangoHrmRequestReceiver.new(receiver_id: tl_admin_hr_id) }
            @employee_leave.is_start_date_enabled = (params[:systango_hrm_employee_leave][:start_date_first_half].present? || params[:systango_hrm_employee_leave][:start_date_second_half].present?) ? 1 : 0
            @employee_leave.is_end_date_enabled = (params[:systango_hrm_employee_leave][:leave_date_first_half].present? || params[:systango_hrm_employee_leave][:leave_date_second_half].present?) ? 1 : 0
            if @employee_leave.save
              @employee_leave.systango_hrm_request_receivers.create(:receiver_id => params[:first_approver].to_i)
              @employee_leave.systango_hrm_request_receivers.create(:receiver_id => params[:final_approver].to_i)
              SystangoHrmMailer.delay.notify_first_approval_for_leave(params[:first_approver], User.current, @employee_leave, params[:systango_hrm_employee_leave][:leave_days])
              if params[:systango_hrm_employee_leave][:available_balance].to_i < params[:systango_hrm_employee_leave][:leave_days].to_i
                flash[:notice	] = "#{params[:systango_hrm_employee_leave][:leave_days].to_i-params[:systango_hrm_employee_leave][:available_balance].to_i} extra leaves will be considered as LWP."
              else
                flash[:notice] = l(:leave_applied_success_notice)
              end
              redirect_to_corresponding_path
            else
              render 'create_update_leaves'
            end
          else
            render 'create_update_leaves'
          end
        else
          flash[:warning] = l(:same_leave_applied_warning_notice)
          redirect_to_corresponding_path
        end
      end
    else
      render 'create_update_leaves'
    end
  end
	
	def edit
  	@employee_leave = SystangoHrmEmployeeLeave.find(params[:id])
		@subject = SystangoHrmSubject.find(@employee_leave.subject_id)
		@current_user_remaining_leaves = SystangoHrmEmployeeLeave.find_all_by_user_id_and_subject_id(User.current.id, @employee_leave.subject_id)
		if @current_user_remaining_leaves.empty?
			@available_balance = @subject.days
		else
			@applied_leaves = 0
			@current_user_remaining_leaves.each do |remaining_leave|
				if remaining_leave.present? && Time.now.year == remaining_leave.leave_end_date.year && Time.now.year == remaining_leave.leave_start_date.year
					@current_user_leave_days = ((remaining_leave.leave_end_date - remaining_leave.leave_start_date).to_i/(60*60*24)) + 1
					@current_user_leave_days -= 0.5 if remaining_leave.is_start_date_enabled
					@current_user_leave_days -= 0.5 if remaining_leave.is_end_date_enabled
					@applied_leaves += @current_user_leave_days
				end
			end
		end
		@available_balance 	= @current_user_remaining_leaves.empty? ? @subject.days : ((@subject.days - @applied_leaves) > 0 ? @subject.days - @applied_leaves : 0)
  	render :template => "systango_hrm_employee_leaves/create_update_leaves"
	end
	
	def update
		if @employee_leave.save
			#To create new Request Receivers
			request_receivers_entry_for_update(@employee_leave,tl_admin_hr_ids)
			send_leave_updated_notification(@employee_leave, tl_admin_hr_ids, (params[:receiver_id][:id] rescue nil), @previous_leave_record) unless date_and_subject_both_updated?(@employee_leave, @previous_leave_record)
  		flash[:notice] = l(:leave_update_success_notice)
  		redirect_to_corresponding_path
		else
			render 'create_update_leaves'
		end
	end

	def show
	end

  def update_leave_status
    employee_leave = SystangoHrmEmployeeLeave.find(params[:id])
    @employee_leave = SystangoHrmEmployeeLeave.find(params[:id])
    #TODO :: Why is this variable named as request_changed_by_user ?? What does it signify? [Palash] : Its show which user is changing status of leave application.
    @request_changed_by_user = SystangoHrmRequestReceiver.by_application_and_receiver_id(params[:id], User.current.id).first
    if params[:cancel]
      first_approver = User.current
      cancel_date = Date.today
      employee_leave.cancel
     SystangoHrmMailer.delay.notify_first_approver_for_cancelled_employee_leave(first_approver, employee_leave, cancel_date)
    else
      unapprove_leave = (params[:unapprove] == SystangoHrm::Constants::STATUS_UNAPPROVED)
      @request_changed_by_user.update_leave_status(unapprove_leave, params[:systango_hrm_employee_leave][:comment])
      # render 'application' and return if @request_changed_by_user.errors.any?
      #render 'application'
    end
    @user_subordinates = SystangoHrmEmployeeLeave.find(params[:id]).user.subordinates
    @teamlead_users = SystangoHrmTeamleadsSubordinates.unlocked_teamleads_for(@user_subordinates)
    @first_approver = (((@teamlead_users.blank? ? [] : @teamlead_users)).uniq).first
    @final_approver = User.users_with_hr_permission.reject(&:blank?).first
    # @manager_approver = User.users_with_manager_permission.reject(&:blank?).first
    if params[:approve]
      if params[:systango_hrm_employee_leave][:comment].blank?
        flash[:warning] = l(:comment_blank_data_error)
        redirect_to application_systango_hrm_employee_leafe_path(employee_leave) and return
      end
      if User.current.id == @first_approver.id
        employee_leave.status = 'first_approved'
        approved_date = Date.today
        first_approver_comment = params[:systango_hrm_employee_leave][:comment] if !params[:systango_hrm_employee_leave][:comment].blank?
        SystangoHrmMailer.delay.notify_final_approval_for_employee_leave(@final_approver.id, employee_leave,approved_date, first_approver_comment)
        SystangoHrmMailer.delay.notify_user_for_leave(employee_leave.user_id, employee_leave,approved_date)
        SystangoHrmMailer.delay.confirm_first_approver_for_leave(User.current, employee_leave,approved_date)
      elsif User.current.id == @final_approver.id
        employee_leave.status = 'approved'
        approved_date = Date.today
        SystangoHrmMailer.delay.notify_user_for_approved_employee_leave(employee_leave.user_id, employee_leave,approved_date)
      end
    else
      if params[:unapprove]
        if params[:systango_hrm_employee_leave][:comment].blank?
         flash[:warning] = l(:comment_blank_data_error)
         redirect_to application_systango_hrm_employee_leafe_path(employee_leave) and return
        end
      employee_leave.status = 'unapproved'
       unapproved_date = Date.today
      current_user = User.current
      first_approver_comment = params[:systango_hrm_employee_leave][:comment] if !params[:systango_hrm_employee_leave][:comment].blank? && params[:unapprove].present?
      SystangoHrmMailer.delay.notify_user_for_unapproved_employee_leave(employee_leave.user_id, employee_leave,first_approver_comment)
      SystangoHrmMailer.delay.notify_first_approver_for_unapproved_employee_leave(current_user, employee_leave, unapproved_date, first_approver_comment)
      end
    end
    flash[:notice] = l(:leave_status_change_notice)
    redirect_to application_systango_hrm_employee_leafe_path(employee_leave)
  end

  ["approved" , "unapproved","pending"].each do |status|
    define_method status do
      @leave_status = status
    if User.current.allowed_to_globally?(:manage_leave_request, {})
      @all_user_leaves = []
      subordinates = User.current.teamleads
      if subordinates.count > 0
        subordinates.each do |subordinate_user|
          leaves = SystangoHrmEmployeeLeave.find_all_by_user_id_and_status(subordinate_user.subordinate_user_id, 'pending').reverse
          @all_user_leaves << leaves
        end
      end
      @all_user_leaves.flatten!
      @all_user_leaves = @all_user_leaves.paginate(:page => params[:page], :per_page => 10) rescue nil
    elsif User.current.hr_user?
      admin_request = SystangoHrmRequestReceiver.where('receiver_approval_status = ? AND receiver_id != ? AND application_id IS NOT NULL','approved',User.current.id) if User.current.hr_user?
      @all_hr_leaves = SystangoHrmRequestReceiver.final_approver_pending_leave.where(application_id: admin_request.pluck(:application_id), receiver_id: User.current.id ).reverse.compact.paginate(:page => params[:page], :per_page => 10) rescue nil
    else
      @all_user_leaves = SystangoHrmEmployeeLeave.where(:user_id=> User.current.id).reverse.compact.paginate(:page => params[:page], :per_page => 10) rescue nil
    end
      render 'more_leaves'
    end
  end

  def application
    if params[:id].present?
      @leave_status = SystangoHrmRequestReceiver.where(receiver_approval_status: ['approved','unapproved'], application_id: params[:id])
    end
    # active_and_admin_users = User.active_users -  [User.current]
    # @teamlead_users = SystangoHrmTeamleadsSubordinates.unlocked_teamleads_for(User.current.subordinates)
    # active_and_admin_users = active_and_admin_users - @teamlead_users if @teamlead_users
    # @teamlead_details = active_and_admin_users.sort_by{|e| e.login.titleize}
    # @default_application_receivers = @teamlead_details
    # @default_application_receivers = @default_application_receivers - [User.current] if (!@users_with_hr_permission.blank? and @users_with_hr_permission.include?(User.current.id))
    # @default_application_receivers = @default_application_receivers.reject(&:blank?)
    @final_approver_request = @leave_detail.systango_hrm_request_receivers.find_by_receiver_id(@users_with_hr_permission)
  end

	def manage_request
	  @pending_leaves = (SystangoHrmRequestReceiver.request_by_user_and_type(User.current, SystangoHrm::Constants::STATUS_PENDING))
	  @approved_leaves = (SystangoHrmRequestReceiver.request_by_user_and_type(User.current, SystangoHrm::Constants::APPROVED))
	  @unapproved_leaves = (SystangoHrmRequestReceiver.request_by_user_and_type(User.current, SystangoHrm::Constants::UNAPPROVED))
	  render 'manage_request'
	end

	def get_leave_balance
		@subject = SystangoHrmSubject.find(params[:leave_id])
    #user_doj_month = User.current.date_of_joining.strftime("%m")
    #user_doj_year = User.current.date_of_joining.strftime("%Y")
		@current_user_remaining_leaves = SystangoHrmEmployeeLeave.find_all_by_user_id_and_subject_id(User.current.id, params[:leave_id])
		if @current_user_remaining_leaves.empty?
      #@available_balance = leaves_calculation_for_user(user_doj_month, user_doj_year, @subject)
			@available_balance = @subject.days
		else
			@applied_leaves = 0
			@leaves_left = 0
			@cancelled_leaves = SystangoHrmEmployeeLeave.find_all_by_user_id_and_subject_id_and_status(User.current.id, params[:leave_id], 'cancel')
			@cancelled_leaves.each do |leave|
				@leaves_left += (leave.leave_end_date.to_date - leave.leave_start_date.to_date).to_i
			end
			@current_user_remaining_leaves.each do |remaining_leave|
				if remaining_leave.present? && Time.now.year == remaining_leave.leave_end_date.year && Time.now.year == remaining_leave.leave_start_date.year
					@current_user_leave_days = ((remaining_leave.leave_end_date - remaining_leave.leave_start_date).to_i/(60*60*24)) + 1
					@current_user_leave_days -= 0.5 if remaining_leave.is_start_date_enabled
					@current_user_leave_days -= 0.5 if remaining_leave.is_end_date_enabled
					@applied_leaves += @current_user_leave_days
				end
			end
		end
		respond_to do |format|
			@available_balance 	= @current_user_remaining_leaves.empty? ? @subject.days : ((@subject.days + (@leaves_left + 1) - @applied_leaves) > 0 ? @subject.days + (@leaves_left + 1) - @applied_leaves : 0)
			format.js
		end
  end

  # def leaves_calculation_for_user(month, year, subject)
  #   if subject.subject.downcase == 'lwp'
  #     leaves = 0
  #   else
  #     leaves_per_month = (subject.days/12.to_f)
  #     leaves = (subject.days - leaves_per_month*(month.to_i - 1))
  #   end
  #   leaves
  # end

private

  #[ Lakhan ]: kept below def because we do not get reciever params in observer.
  def send_leave_updated_notification(employee_leave, tl_admin_hr_ids, receivers =[], previous_leave_record = nil)
		tl_admin_hr_mails = User.get_email_for_users((tl_admin_hr_ids - receivers.map(&:to_i)))
		#TODO:: Why are we sending previous_leave_record.attributes ? #[Palash] : Because we have to send email to old leave thread that leave has been update.
    SystangoHrmMailer.delay.notify_all_of_leave_updation(tl_admin_hr_mails, previous_leave_record.attributes, employee_leave) 
  end

	def tl_admin_hr_ids
		tl_admin_hr_ids = @teamlead_users.map(&:id) + User.admin_users.map(&:id)
		if params[:apply][:leave] == (SystangoHrm::Constants::SELF).downcase and (@users_with_hr_permission.include?(User.current.id))
			tl_admin_hr_ids -= [User.current.id] 
		elsif params[:apply][:leave] == SystangoHrm::Constants::REFER 
			#[Palash]: If HR leave is reffered then in request recievers entry of HR is also there because User.admin_user find HR.
			tl_admin_hr_ids -= [@employee_leave.referral_id]  if (@users_with_hr_permission.include?(@employee_leave.referral_id))
			tl_admin_hr_ids << User.current.id
		end
		tl_admin_hr_ids += (params[:receiver_id][:id][1..-1].map(&:to_i) rescue [])
		tl_admin_hr_ids.uniq rescue []
	end
  
  def redirect_to_corresponding_path
  	redirect_to new_systango_hrm_employee_leafe_path and return if (params[:continue] || params[:view])
  	#redirect_to new_systango_hrm_employee_leafe_path and return if params[:continue]
    #redirect_to leaves_manage_path and return if (params[:apply][:leave] == (SystangoHrm::Constants::REFER) and params[:view])
    redirect_to report_self_path
  end
  
  def initialize_employee_leave
    @previous_leave_record = SystangoHrmEmployeeLeave.find(params[:id])
		@employee_leave.user_id = User.current.id if @employee_leave.referral_id.blank?
    @employee_leave.referral_id = params[:systango_hrm_employee_leave][:referral_id] if params[:apply][:leave] == (SystangoHrm::Constants::REFER)
		@employee_leave.attributes = params[:systango_hrm_employee_leave]
  end

  def find_or_initialize_employee_leave
    @employee_leave = SystangoHrmEmployeeLeave.new if params[:action] == 'create'
    @employee_leave = SystangoHrmEmployeeLeave.find(params[:id]) if params[:action] == 'update'
  end

  def leave_application_details
    leave_application_instance
    user_ids = (@default_application_receivers + @users_with_hr_permission.to_a).map{|user| user.id}
    user_ids.each do |user|
      @request_recievers_for_leave = SystangoHrmRequestReceiver.by_application_and_receiver_id(params[:id].to_i, user)
    end
    @request_reciever = SystangoHrmRequestReceiver.by_application_and_receiver_id(params[:id].to_i, User.current.id).first
  end

  def fetch_teamleads_subordinates
    @teamlead_users = SystangoHrmTeamleadsSubordinates.unlocked_teamleads_for(User.current.subordinates)
    @subordinate_users = SystangoHrmTeamleadsSubordinates.unlocked_subordinates_for(User.current.teamleads)
  end
  
  def check_leave_account_is_created
		@leave_account = User.current.systango_hrm_leave_account
    if @leave_account.blank?
      flash.now[:error] = l(:leave_account_not_activated_error)
      render 'create_update_leaves'
    end
  end

  def leave_application_instance
    #admins = User.admin_users
    #active_and_admin_users = User.active_users - admins - [User.current]
    active_and_admin_users = User.active_users - [User.current]
    active_and_admin_users = active_and_admin_users - @teamlead_users if @teamlead_users
	  @teamlead_details = active_and_admin_users.sort_by{|e| e.firstname.titleize}
    #@default_application_receivers = (admins + (@teamlead_users.blank? ? [] : @teamlead_users)).uniq
    @default_application_receivers = ((@teamlead_users.blank? ? [] : @teamlead_users)).uniq
		hr_permission_users = User.users_with_hr_permission.reject(&:blank?)
		if hr_permission_users.present?
			@users_with_hr_permission = hr_permission_users
    end
    @default_application_receivers = @default_application_receivers - [User.current] if (!@users_with_hr_permission.blank? and @users_with_hr_permission.include?(User.current.id))
    @default_application_receivers = @default_application_receivers.reject(&:blank?)
		subordinates_with_leave_account = (User.current.teamleads.collect{|teamlead| teamlead if teamlead.subordinate_user.systango_hrm_leave_account}).compact
		@subordinates_with_leave_account = SystangoHrmTeamleadsSubordinates.unlocked_subordinates_for(subordinates_with_leave_account)
		#@subjects = SystangoHrmSubject.all_as_array
		@subjects = SystangoHrmSubject.all(conditions: ['is_deleted', false])
  end

  #[Palash]: Validation is put here because on model we are unable to decide type of leave.
	# def validate_leave_type
	# 	if (params[:systango_hrm_employee_leave] and params[:apply][:leave] == SystangoHrm::Constants::REFER and params[:systango_hrm_employee_leave][:referral_id].blank?)
	# 		flash.now[:error] = l(:leave_referral_not_selected_error)
	# 		render 'create_update_leaves'
	# 	end
	# end

  def authorize_to_view_application
    @leave_detail = SystangoHrmEmployeeLeave.find(params[:id]) rescue nil
    # if @leave_detail.blank? or (SystangoHrmRequestReceiver.by_application_and_receiver_id(params[:id], User.current.id).blank? and !(User.current.id == (@leave_detail.applied_user.id)))
    if @leave_detail.blank?
      flash[:error] = l(:application_view_authorisation_error)
      redirect_to home_path
    end
  end

	def date_and_subject_both_updated?(employee_leave,previous_leave_record)
		(previous_leave_record.subject_id == employee_leave.subject_id) and (previous_leave_record.leave_start_date.to_date == employee_leave.leave_start_date.to_date) and (previous_leave_record.leave_end_date.to_date == employee_leave.leave_end_date.to_date)
	end

	def request_receivers_entry_for_update(employee_leave,tl_admin_hr_ids)
		return SystangoHrmRequestReceiver.update_request_receivers(employee_leave,tl_admin_hr_ids) if !params[:receiver_id][:id].reject(&:empty?).blank?
		SystangoHrmRequestReceiver.change_status_to_pending_for_all_receivers(params[:id])
  end

  def initialize_admin_approved_leave
    admin_request = SystangoHrmRequestReceiver.where('receiver_approval_status = ? AND receiver_id != ?','approved',User.current.id) if User.current.hr_user?
    if admin_request.present? && User.current.hr_user?
      @pending_leave = SystangoHrmRequestReceiver.final_approver_pending_leave.where(application_id: admin_request.pluck(:application_id), receiver_id: User.current.id )
    end
  end
end
