class SystangoHrmHolidaysController < SystangoHrmController
  unloadable

  helper :custom_fields
  helper :context_menus unless Redmine::VERSION.to_s < '1.4'
  include CustomFieldsHelper

  include SystangoHrm::ContextMenu
  generate_context_menu("holiday")

  before_filter :find_holidays, :only => [:index, :create, :calendar]
  before_filter :initialize_params, :only => [:index, :create]
  
  def calendar
  end

  def index
		@holiday_tab = session.delete(:holiday_tab)
		conditions = params[:non_working_day_year].present? ? ["non_working_day_year = ?", params[:non_working_day_year]] : ["non_working_day_year = ?", Date.today.year]
		@non_working_days = SystangoHrmNonWorkingDay.find(:all, :conditions => conditions, :order => "id ASC")
		@nwd_year = SystangoHrmNonWorkingDay.order('non_working_day_year ASC').all.map{|nwd| nwd.non_working_day_year}.uniq.first
		if params[:non_working_day_year].present?
			respond_to do |format|
				format.js
			end
		end
  end

	def create
		session[:holiday_tab] = true
		if @holiday.valid?
			@holiday.save
			flash[:notice] = l(:holiday_created_notice)
			redirect_to systango_hrm_holidays_path
		else
			@holiday_tab = session.delete(:holiday_tab)
			conditions = params[:non_working_day_year].present? ? ["non_working_day_year = ?", params[:non_working_day_year]] : ["non_working_day_year = ?", Date.today.year]
			@non_working_days = SystangoHrmNonWorkingDay.find(:all, :conditions => conditions, :order => "id ASC")
			render 'index'
		end
	end

	def edit
		@holiday = SystangoHrmHoliday.find(params[:id])
	end

	def update
		@holiday = SystangoHrmHoliday.find(params[:holiday][:id])
		@holiday.attributes = params[:holiday]
		if @holiday.save
			flash[:notice] = l(:holiday_update_notice)
			redirect_to systango_hrm_holidays_path
			session[:holiday_tab] = true
		else
			render 'edit'
		end
	end

private

  def find_holidays
	 	@holidays = SystangoHrmHoliday.order_by_holiday_desc
  end
  
  def initialize_params
    @holiday = SystangoHrmHoliday.new(params[:holiday])
  end

end
