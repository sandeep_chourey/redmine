class SystangoHrmCompoffsController < SystangoHrmController
  unloadable
  before_filter :compoff_instance
	before_filter :initialize_admin_approved_leave, only: :index
	before_filter :authorize_to_view_application, only: [:application,:update_leave_status]
	before_filter :leave_application_details, only: [:new, :create, :edit, :update, :application, :accepted, :unapproved, :pending, :manage_request, :update_leave_status, :get_leave_balance]
	before_filter :leave_application_instance, only: [:new, :create, :manage_request, :edit, :update, :application]

	def index
		if User.current.allowed_to_globally?(:manage_leave_request, {})
			@compoffs = SystangoHrmCompoff.where('user_id != ?', User.current.id)
		else
			@compoffs = SystangoHrmCompoff.find_all_by_user_id(User.current.id)
		end
	end

  def new
		@compoff = SystangoHrmCompoff.new
		@teamlead_users = SystangoHrmTeamleadsSubordinates.unlocked_teamleads_for(User.current.subordinates)
		@default_application_receivers = ((@teamlead_users.blank? ? [] : @teamlead_users)).uniq
  end

	def create
  	#@compoffs = User.find(params[:user_id]).systango_hrm_compoffs.order_by_created_desc rescue nil
  	#@compoffs = SystangoHrmCompoff.new(params[:systango_hrm_compoff])
		# check whether form is submitted by javascript or by submit button and to display the record of selected user.
		render 'new' and return if params[:commit].blank?
	  @compoff = SystangoHrmCompoff.new(params[:systango_hrm_compoff])
		@compoff.status = "pending"
	  if @compoff.save
			@compoff.systango_hrm_request_receivers.create(:receiver_id => params[:first_approver].to_i)
			leave_days = ((@compoff.leave_end_date.to_date -  @compoff.leave_start_date.to_date).to_i + 1)
			#pl_leave = SystangoHrmSubject.find_by_subject('PL')
			#pl_leave.days = pl_leave.days + leave_days
			#pl_leave.save
			#@pl_leave.days = @pl_leave.days + params[:systango_hrm_compoff][:systango_hrm_compoff].to_i
			SystangoHrmMailer.delay.notify_first_approval_for_comp_off_leave(params[:first_approver].to_i, User.current, @compoff)
			SystangoHrmMailer.delay.notify_compoff_added_to_employee(@compoff, User.current)
      flash[:notice] = l(:leave_compoff_added_notice)
	    redirect_to new_systango_hrm_compoff_url
	  else
      render 'new'
	  end
	end

	def edit
		@compoff = SystangoHrmCompoff.find(params[:id])
	end

	def update_compoff_leave_status
		employee_leave = SystangoHrmCompoff.find(params[:id])
		@request_changed_by_user = SystangoHrmRequestReceiver.by_compoff_and_receiver_id(params[:id], User.current.id).first
		unless params[:cancel]
			unapprove_leave = (params[:unapprove] == SystangoHrm::Constants::STATUS_UNAPPROVED)
			@request_changed_by_user.update_compoff_leave_status(unapprove_leave, params[:systango_hrm_compoffs][:comment])
			# render 'application' and return if @request_changed_by_user.errors.any?
			#render 'application'
		end
		@user_subordinates = SystangoHrmCompoff.find(params[:id]).user.subordinates
		@teamlead_users = SystangoHrmTeamleadsSubordinates.unlocked_teamleads_for(@user_subordinates)
		@first_approver = (((@teamlead_users.blank? ? [] : @teamlead_users)).uniq).first
		if params[:approve]
			if params[:systango_hrm_compoffs][:comment].blank?
				flash[:warning] = l(:comment_blank_data_error)
				redirect_to application_systango_hrm_compoff_path(employee_leave) and return
			end
			if User.current.id == @first_approver.id
				employee_leave.status = 'approved'
				approved_date= Date.today
				employee_leave.save
				leave_days = ((employee_leave.leave_end_date.to_date -  employee_leave.leave_start_date.to_date).to_i + 1)
				pl_leave = SystangoHrmSubject.find_by_subject('PL') if !pl_leave.nil?
				pl_leave.days = pl_leave.days + leave_days  if !pl_leave.nil?
				pl_leave.save  if !pl_leave.nil?
				#SystangoHrmMailer.delay.notify_final_approval_for_employee_special_leave(@final_approver.id, employee_leave)
				#SystangoHrmMailer.delay.notify_user_for_special_leave(employee_leave.user_id, employee_leave)
				SystangoHrmMailer.delay.notify_user_for_approved_compoff_leave(employee_leave.user_id, employee_leave, approved_date)
			end
		elsif params[:cancel]
			receivers = @teamlead_users.first.mail.to_a
			employee_leave.cancel_status
			employee_leave.systango_hrm_request_receivers.first.cancel_status unless employee_leave.systango_hrm_request_receivers.blank?
			first_approver = employee_leave.systango_hrm_request_receivers.first.user.mail.to_s
			employee_leave.save
			leave_days = ((employee_leave.leave_end_date.to_date -  employee_leave.leave_start_date.to_date).to_i + 1)
			# pl_leave = SystangoHrmSubject.find_by_subject('PL')
			# pl_leave.days = pl_leave.days - leave_days
			# pl_leave.save
			canceled_date = Date.today
			SystangoHrmMailer.delay.notify_user_of_compoff_leave_cancelled(employee_leave, first_approver, receivers, canceled_date)
	else
		if params[:systango_hrm_compoffs][:comment].blank?
			flash[:warning] = l(:comment_blank_data_error)
			redirect_to application_systango_hrm_compoff_path(employee_leave) and return
		end
			employee_leave.status = 'unapproved'
		unapproved_date= Date.today
			employee_leave.save
			SystangoHrmMailer.delay.notify_user_for_unapproved_compoff_leave(employee_leave.user_id, employee_leave, unapproved_date)
		end
		flash[:notice] = l(:leave_status_change_notice)
		redirect_to application_systango_hrm_compoff_path(employee_leave)
	end

		["approved" , "unapproved","pending"].each do |status|
		define_method status do
			@leave_status = status
			if params[:category] =='compoff_request' && params[:action] == 'pending' && User.current.allowed_to_globally?(:manage_leave_request, {})
			#	@compoff_leaves = SystangoHrmRequestReceiver.request_by_user_and_type_for_compoff_leave(User.current, 'pending').reverse.compact.paginate(:page => params[:page], :per_page => 10) rescue nil
			@compoff_leaves =  SystangoHrmCompoff.where('user_id = ? AND status= ?',User.current.id,'pending').reverse.compact.paginate(:page => params[:page], :per_page => 10) rescue nil
			elsif params[:category] =='compoff_request' && params[:action] == 'pending' &&  !User.current.admin_user? && !User.current.allowed_to_globally?(:manage_leave_request, {})
			 @compoff_leaves = SystangoHrmCompoff.where('user_id = ? AND status= ?',User.current.id,'pending').reverse.compact.paginate(:page => params[:page], :per_page => 10) rescue nil
			  #@compoff_leaves = SystangoHrmRequestReceiver.request_by_user_and_type_for_compoff_leave(User.current, 'pending').compact.paginate(:page => params[:page], :per_page => 10) rescue nil
			elsif params[:category] =='compoff_pending_request' && params[:action] == 'pending'
				teamlead_users = SystangoHrmTeamleadsSubordinates.find_all_by_teamlead_user_id(User.current.id).map{|tluser| tluser.subordinate_user_id}  if User.current.present?
				#@compoff_pending_leaves = SystangoHrmCompoff.where('user_id != ? AND status= ?',User.current.id,'pending').compact.paginate(:page => params[:page], :per_page => 10) rescue nil
				@compoff_pending_leaves = SystangoHrmCompoff.where('user_id IN (?) AND status= ?',teamlead_users,'pending').reverse.compact.paginate(:page => params[:page], :per_page => 10) rescue nil
			else
				@compoff_leaves = SystangoHrmRequestReceiver.request_by_user_and_type_for_compoff_leave(User.current, @leave_status).reverse.paginate(:page => params[:page], :per_page => 10) rescue nil
			end
			render 'more_leaves'
		end
	end

	def application
		if params[:id].present?
			@leave_status = SystangoHrmRequestReceiver.where(receiver_approval_status: ['approved','unapproved'], comp_off_application_id: params[:id])
		end
		@final_approver_request = @leave_detail.systango_hrm_request_receivers.find_by_receiver_id(@users_with_hr_permission)
	end

	def update
		# @teamlead_users = SystangoHrmTeamleadsSubordinates.unlocked_teamleads_for(User.current.subordinates)
		# @first_approver = (((@teamlead_users.blank? ? [] : @teamlead_users)).uniq).first
		# @final_approver = User.users_with_hr_permission.reject(&:blank?).first
		# @compoff = SystangoHrmCompoff.find(params[:id])
		# @compoff.attributes = params[:compoff]
		# if params[:commit] == "Approve"
		# 	if User.current.id == @first_approver.id
		# 		@compoff.status = 'first_approved'
		# 	else
		# 		@compoff.status = 'approved'
		# 	end
		# else
		# 	@compoff.status = 'unapproved'
		# end
		# if @compoff.save
		# 	if @compoff.status == 'first_approved'
		# 		SystangoHrmMailer.delay.notify_final_approval_for_compoff_leave(@final_approver.id, @compoff)
		# 		SystangoHrmMailer.delay.notify_user_for_compoff_leave(@compoff.user_id, @compoff)
		# 	elsif @compoff.status == 'approved'
		# 		SystangoHrmMailer.delay.notify_user_for_approved_compoff_leave(@compoff.user_id, @compoff)
		# 	elsif @compoff.status == 'unapproved'
		# 		SystangoHrmMailer.delay.notify_user_for_unapproved_compoff_leave(@compoff.user_id, @compoff)
		# 	end
		# 	redirect_to systango_hrm_compoffs_path
		# else
		# 	render 'edit'
		# end
		@compoff = SystangoHrmCompoff.find(params[:id])
		@compoff.attributes = params[:compoff]
		if @compoff.save
			flash[:notice] = l(:compoff_update_notice)
			redirect_to systango_hrm_compoffs_path
		else
			render 'edit'
		end
	end

	def destroy
		@compoff = SystangoHrmCompoff.find(params[:id])
		@compoff.destroy
		flash[:notice] = l(:compoff_delete_notice)
		redirect_to systango_hrm_compoffs_path
	end

private

	def initialize_admin_approved_leave
		admin_request = SystangoHrmRequestReceiver.where('receiver_approval_status = ? AND receiver_id != ?','approved',User.current.id) if User.current.hr_user?
		if admin_request.present? && User.current.hr_user?
			@pending_leave = SystangoHrmRequestReceiver.final_approver_pending_leave.where(special_request_application_id: admin_request.pluck(:special_request_application_id), receiver_id: User.current.id )
		end
	end

	def compoff_instance
    @users = SystangoHrmLeaveAccount.with_active_user.map(&:user).compact.sort_by(&:firstname) rescue nil
	end

	def authorize_to_view_application
		@leave_detail = SystangoHrmCompoff.find(params[:id]) rescue nil
		# if @leave_detail.blank? or (SystangoHrmRequestReceiver.by_application_and_receiver_id(params[:id], User.current.id).blank? and !(User.current.id == (@leave_detail.applied_user.id)))
		if @leave_detail.blank?
			flash[:error] = l(:application_view_authorisation_error)
			redirect_to home_path
		end
	end

	def leave_application_instance
		#admins = User.admin_users
		#active_and_admin_users = User.active_users - admins - [User.current]
		active_and_admin_users = User.active_users - [User.current]
		active_and_admin_users = active_and_admin_users - @teamlead_users if @teamlead_users
		@teamlead_details = active_and_admin_users.sort_by{|e| e.firstname.titleize}
		#@default_application_receivers = (admins + (@teamlead_users.blank? ? [] : @teamlead_users)).uniq
		@default_application_receivers = ((@teamlead_users.blank? ? [] : @teamlead_users)).uniq
		hr_permission_users = User.users_with_hr_permission.reject(&:blank?)
		if hr_permission_users.present?
			@users_with_hr_permission = hr_permission_users
		end
		@default_application_receivers = @default_application_receivers - [User.current] if (!@users_with_hr_permission.blank? and @users_with_hr_permission.include?(User.current.id))
		@default_application_receivers = @default_application_receivers.reject(&:blank?)
		subordinates_with_leave_account = (User.current.teamleads.collect{|teamlead| teamlead if teamlead.subordinate_user.systango_hrm_leave_account}).compact
		@subordinates_with_leave_account = SystangoHrmTeamleadsSubordinates.unlocked_subordinates_for(subordinates_with_leave_account)
		#@subjects = SystangoHrmSubject.all_as_array
		@subjects = SystangoHrmSubject.all(conditions: ['is_deleted', false])
	end


	def leave_application_details
		leave_application_instance
		user_ids = (@default_application_receivers.to_a).map{|user| user.id}
		user_ids.each do |user|
			@request_recievers_for_leave = SystangoHrmRequestReceiver.by_compoff_and_receiver_id(params[:id].to_i, user)
		end
		@request_reciever = SystangoHrmRequestReceiver.by_compoff_and_receiver_id(params[:id].to_i, User.current.id).first
	end

end
