class SystangoHrmSubject < SystangoHrmModel

  unloadable
  include SystangoHrm::CachedModel
  validates :subject,
          	:presence => {:message => l(:subject_presence_true_error)}
  validates :days,
            :presence => {:message => l(:days_presence_true_error)}
	
  belongs_to :systango_hrm_employee_leave, :foreign_key => "id"
  
  default_scope  {order('subject asc')}

  def get_leave_balance(year)
    @subject = SystangoHrmSubject.find(self.id)
    @current_user_remaining_leaves = SystangoHrmEmployeeLeave.find(:all, :conditions => ["user_id = ? and subject_id = ? and (YEAR(leave_start_date) = ? OR YEAR(leave_end_date) = ?)", User.current.id, self.id, year, year])
    if @current_user_remaining_leaves.empty?
      @available_balance = @subject.days
    else
      @applied_leaves = 0
      @current_user_remaining_leaves.each do |remaining_leave|
        if remaining_leave.present? && (year.to_i == remaining_leave.leave_end_date.year) && (year.to_i == remaining_leave.leave_start_date.year)
          @current_user_leave_days = ((remaining_leave.leave_end_date - remaining_leave.leave_start_date).to_i/(60*60*24)) + 1
          @current_user_leave_days -= 0.5 if remaining_leave.is_start_date_enabled
          @current_user_leave_days -= 0.5 if remaining_leave.is_end_date_enabled
          @applied_leaves += @current_user_leave_days
        end
      end
    end
    @available_balance 	= @current_user_remaining_leaves.empty? ? @subject.days : ((@subject.days - @applied_leaves) > 0 ? @subject.days - @applied_leaves : 0)
  end

  def total_days
    SystangoHrmSubject.find_by_id(self.id).days
  end

end
