class SystangoHrmNonWorkingDay < ActiveRecord::Base
  unloadable

  WEEK_DAYS = [["Monday", "Monday"], ["Tuesday", "Tuesday"], ["Wednesday", "Wednesday"], ["Thursday", "Thursday"], ["Friday", "Friday"], ["Saturday", "Saturday"], ["Sunday", "Sunday"]]
  LEAVE_CONSIDERED = [["Full day Leave", "Full day Leave"], ["No leave", "No leave"]]
  NON_WORKING_DAYS = [["Even week days", "Even week days"], ["Odd week days", "Odd week days"], ["All week days", "All week days"]]
end
