class SystangoHrmCompoff < SystangoHrmModel
  unloadable
  belongs_to :user
  has_many :systango_hrm_request_receivers, :foreign_key => 'comp_off_application_id', :class_name => "SystangoHrmRequestReceiver", dependent: :destroy
  
  validates :comp_off_remarks, :presence => {:message => l(:compoff_remarks_presence_true_error)}
  validates :user_id, :presence =>{:message => l(:leave_select_employee_name_error)}
  # validates :leave_start_date, :presence => {:message => l(:leave_start_date_not_added_error)}, :format => { :with => SystangoHrm::Constants::DATETIME_FORMAT_REGEX, :message=> l(:proper_date_error_message) }
  validates :leave_start_date, :presence => {:message => l(:leave_start_date_not_added_error)}
  # validates :leave_end_date, :presence => {:message => l(:leave_end_date_not_added_error)}, :format => { :with => SystangoHrm::Constants::DATETIME_FORMAT_REGEX,:message=> l(:proper_date_error_message) }
  #validates :leave_end_date, :presence => {:message => l(:leave_end_date_not_added_error)}, :format => { :with => SystangoHrm::Constants::DATETIME_FORMAT_REGEX,:message=> l(:proper_date_error_message) }

  validate :validate_compoff_given

  scope :by_user_id, ->(user_id) { where(user_id: user_id) }
  scope :order_by_created_desc, ->{order("created_at desc")}
  scope :by_application_and_receiver_id, ->(comp_off_application_id, receiver_id) { where(comp_off_application_id: comp_off_application_id, receiver_id: receiver_id) }

  def validate_compoff_given
    return errors.add(:comp_off_given, l(:compoff_blank_error)) if comp_off_given.blank?
    return errors.add(:comp_off_given, l(:compoff_zero_error_message)) if comp_off_given == 0
    return errors.add(:comp_off_given, l(:compoff_negative_error)) if comp_off_given <= 0
    return errors.add(:comp_off_given, l(:compoff_decimal_error_message)) unless comp_off_given % 0.5 == 0
    return errors.add(:comp_off_given, l(:comp_off_remarks)) unless !comp_off_remarks.blank?
  end

  def cancel?
    self.status == 'cancel'
  end

  def pending?
    self.status == 'pending'
  end

  def unapproved?
    self.status == 'unapproved'
  end

  def cancel_status
    self.status = 'cancel'
    self.save
  end

  def applied_user
    self.user
  end

end
