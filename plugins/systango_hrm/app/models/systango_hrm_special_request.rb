class SystangoHrmSpecialRequest < SystangoHrmModel
  unloadable

  belongs_to :user
  has_many :systango_hrm_request_receivers, :foreign_key => 'special_request_application_id', dependent: :destroy
  validates :leave_start_date,
            :presence => {message: l(:special_request_leave_date)}

  validates :hours_minutes,
            :presence => {message: l(:special_request_hours_and_minutes)}

  validates :reason,
            :presence => {message: l(:special_request_reason)}

  STATUSES = [['Pending', 'pending'], ['Approved', 'approved'], ['Unapproved', 'unapproved'], ['Cancel', 'cancel']]

  scope :pending_leave, -> {where(status:'pending')}


  def applied_user
    self.user
  end

  def cancel?
    self.status == 'cancel'
  end

  def unapproved?
    self.status == 'unapproved'
  end

  def cancel_status
    self.status = 'cancel'
    self.save
  end
end
