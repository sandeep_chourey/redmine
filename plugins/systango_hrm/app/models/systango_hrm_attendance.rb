class SystangoHrmAttendance < ActiveRecord::Base
  unloadable
  self.table_name = "employee_attendance"

  belongs_to :user, :foreign_key => 'user_id', :class_name => "User"

  # scope :order_by_user_name, -> {order("user.name desc")}

end
