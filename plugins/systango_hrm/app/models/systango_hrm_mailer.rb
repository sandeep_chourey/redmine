class SystangoHrmMailer < Mailer

	def notify_compoff_added_to_employee(compoff, current_user)
		@compoff, @user = compoff, current_user
		subject = "Comp-off leave applied from #{@compoff.leave_start_date.strftime("%d/%m/%Y")} to #{@compoff.leave_end_date.strftime("%d/%m/%Y")}"
		mail(to: @compoff.user.mail, subject: subject )
	end

	def notify_employee_of_their_leave(leave,referred_by)
		@leave, @referred_by = leave, referred_by
		subject = "#{@leave.systango_hrm_subject.subject} applied from #{@leave.leave_start_date.strftime("%d/%m/%Y")} to #{@leave.leave_end_date.strftime("%d/%m/%Y")}"
		mail(to: @leave.applied_user.mail, subject: subject)
	end

	def notify_first_approver_for_cancelled_employee_leave(current_user,employee_leave, cancel_date)
		@leave_details = employee_leave
		@user = current_user
		@cancel_date = Date.today
		subject = "#{@leave_details.user.name} cancelled the leave"
		mail(to: @user.mail, subject: subject)
	end

	def notify_employee_of_their_special_leave(current_user, employee_leave)
		@leave_details = employee_leave
		@user = current_user
		subject = "Special Request applied for #{@leave_details.leave_start_date.strftime("%d/%m/%Y")}"
		mail(to: @user.mail, subject: subject)
	end

	def send_reminder_email(leave_details, receivers_emails)
		@leave = leave_details
		subject = "#{@leave.systango_hrm_subject.subject} applied by #{@leave.applied_user.name.titleize rescue nil} from- #{@leave.leave_start_date.strftime("%b %d, %Y")} to- #{@leave.leave_end_date.strftime("%b %d, %Y")}"
		mail(to: receivers_emails, subject: subject)
	end

	def notify_admin_tls_of_leave_request(receiver_ids, leave)
		@leave = leave
		subject = "#{@leave.systango_hrm_subject.subject} applied by #{@leave.applied_user.name.titleize rescue nil} from- #{@leave.leave_start_date.strftime("%b %d, %Y")} to- #{@leave.leave_end_date.strftime("%b %d, %Y")}"
		email_to = (User.get_email_for_users(receiver_ids)).join(',')
		mail(to: email_to, subject: subject)
	end

	def notify_all_of_leave_processing(receiver_ids, leave)
		@leave = leave
		subject = "#{@leave.systango_hrm_subject.subject} applied by #{@leave.applied_user.name.titleize rescue nil} from- #{@leave.leave_start_date.strftime("%b %d, %Y")} to- #{@leave.leave_end_date.strftime("%b %d, %Y")}"
		email_to = (User.get_email_for_users(receiver_ids)).join(',')
		mail(to: email_to, subject: subject)
	end

	def notify_employee_of_leave_processing(leave, referred_by)
		@leave, @referred_by = leave, referred_by
		subject = "#{@leave.systango_hrm_subject.subject} applied by #{@leave.applied_user.name.titleize rescue nil} from- #{@leave.leave_start_date.strftime("%b %d, %Y")} to- #{@leave.leave_end_date.strftime("%b %d, %Y")}"
		mail(to: @leave.applied_user.mail, subject: subject)
	end

	def notify_subordinate_of_tl_updation(present_teamleads, teamlead_and_subordinate_id, is_added=false)
		@present_teamleads, @teamlead, @is_subordinate_added = present_teamleads, User.find(teamlead_and_subordinate_id[:teamlead_id]), is_added
		mail(to: User.find(teamlead_and_subordinate_id[:subordinate_id]).mail, subject: l(:mailer_subject_subordinate_update))
	end

	def notify_tl_of_subordinate_updation(teamlead_and_subordinate_id, for_admin=false, is_subordinate_added=false)
		@teamlead_user, @subordinate_user, @is_added =  User.find(teamlead_and_subordinate_id[:teamlead_id]), User.find(teamlead_and_subordinate_id[:subordinate_id]), is_subordinate_added
		mail_to = (for_admin ? User.admin_users.map(&:mail) : @teamlead_user.mail)
		mail_subject = (for_admin ? l(:mailer_subject_teamlead_subordinate_update) : l(:mailer_subject_teamlead_update))
		mail(to: mail_to, subject: mail_subject)
	end

	def notify_all_of_leave_updation(receivers_emails, leave_details, new_leave_details)
		@updated_leave = new_leave_details
		# we are not appliying association here because we are passing hash and that's why we put our business logic here
		@user = leave_details["referral_id"].blank? ? (User.find(leave_details["user_id"]) rescue nil) : (User.find(leave_details["referral_id"]) rescue nil)
		subject = "#{SystangoHrmSubject.find(leave_details["subject_id"]).subject} applied by #{@user.name.titleize} from- #{leave_details["leave_start_date"].strftime("%b %d, %Y")} to- #{leave_details["leave_end_date"].strftime("%b %d, %Y")}"
		email_to = (receivers_emails << @user.mail).join(',')
		mail(to: email_to, subject: subject)
	end

	def notify_first_approval_for_leave(first_approver_id, current_user, employee_leave, leave_days)
		@leave_details, @user, @leave_days = employee_leave, current_user, leave_days
		@receivers_details = User.find(first_approver_id)
		@user = current_user
		subject = "#{current_user.name} applied for leave"
		mail(to: @receivers_details.mail, subject: subject)
	end

	def notify_first_approval_for_special_leave(first_approver_id, current_user, employee_leave)
		@leave_details, @user = employee_leave, current_user
		@receivers_details = User.find(first_approver_id)
		@user = current_user
		subject = "#{current_user.name} applied for Special Request"
		mail(to: @receivers_details.mail, subject: subject)
	end

	def notify_first_approval_for_comp_off_leave(first_approver_id, current_user, employee_leave)
		@leave_details, @user = employee_leave, current_user
		@receivers_details = User.find(first_approver_id)
		@user = current_user
		subject = "#{@user.name} applied for Comp-Off leave"
		mail(to: @receivers_details.mail, subject: subject)
	end

	def notify_final_approval_for_compoff_leave(user_id, compoff)
		@receivers_details = User.find(user_id)
		@compoff = compoff
		subject = "#{compoff.user.name} applied for compoff leave and approved by first approver"
		mail(to: @receivers_details.mail, subject: subject)
	end

	def notify_user_for_compoff_leave(user_id, compoff)
		@receivers_details = User.find(user_id)
		@compoff = compoff
		subject = "Compoff Leave approved by first approver"
		mail(to: @receivers_details.mail, subject: subject)
	end

	def notify_user_for_approved_compoff_leave(user_id, compoff,approved_date)
		@receivers_details = User.find(user_id)
		@compoff = compoff
		@approved_date = Date.today
		subject = "Compoff Leave approved by #{@compoff.systango_hrm_request_receivers.first.user.name}"
		mail(to: @receivers_details.mail, subject: subject)
	end

	def notify_user_for_unapproved_compoff_leave(user_id, compoff, unapproved_date)
		@receivers_details = User.find(user_id)
		@compoff = compoff
		@unapproved_date = Date.today
		subject = "Compoff Leave unapproved by #{@compoff.systango_hrm_request_receivers.first.user.name}"
		mail(to: @receivers_details.mail, subject: subject)
	end

	def notify_final_approval_for_employee_leave(user_id, employee_leave, approved_date,first_approver_comment)
		@receivers_details = User.find(user_id)
		@employee_leave = employee_leave
		@approved_date = Date.today
		@first_approver_comment = first_approver_comment
		subject = "Leave approved by first approver"
		mail(to: @receivers_details.mail, subject: subject)
	end

	def notify_final_approval_for_employee_special_leave(user_id, employee_leave)
		@receivers_details = User.find(user_id)
		@employee_leave = employee_leave
		@approved_date = Date.today
		subject = "Special Request approved by first approver"
		mail(to: @receivers_details.mail, subject: subject)
	end

	def notify_user_for_leave(user_id, employee_leave, approved_date)
		@receivers_details = User.find(user_id)
		@employee_leave = employee_leave
		@approved_date = Date.today
		subject = "Leave approved by first approver"
		mail(to: @receivers_details.mail, subject: subject)
	end

	def confirm_first_approver_for_leave(user,employee_leave, approved_date)
		  @user = user
			@employee_leave = employee_leave
		  @approved_date = Date.today
			subject = "#{@employee_leave.user.name} Leave approved"
			mail(to: @user.mail, subject: subject)
	end

	def notify_user_for_special_leave(user_id, employee_leave)
		@receivers_details = User.find(user_id)
		@employee_leave = employee_leave
		@approved_date = Date.today
		subject = "Leave approved by first approver"
		mail(to: @receivers_details.mail, subject: subject)
	end

	def notify_user_for_approved_employee_leave(user_id, employee_leave, approved_date)
		@receivers_details = User.find(user_id)
		@employee_leave = employee_leave
		@approved_date = Date.today
		subject = "Leave approved by final approver."
		mail(to: @receivers_details.mail, subject: subject)
	end

	def notify_user_for_approved_employee_special_leave(user_id, employee_leave)
		@receivers_details = User.find(user_id)
		@employee_leave = employee_leave
		@approved_date = Date.today
		subject = "Special Request approved by final approver."
		mail(to: @receivers_details.mail, subject: subject)
	end

	def notify_user_for_unapproved_employee_leave(user_id, employee_leave,first_approver_comment)
		@receivers_details = User.find(user_id)
		@employee_leave = employee_leave
		@unapproved_date = Date.today
		@first_approver_comment = first_approver_comment
		subject = "Leave unapproved by first approver"
		mail(to: @receivers_details.mail, subject: subject)
	end

	def notify_user_for_unapproved_special_leave(user_id, employee_leave,first_approver_comment)
		@receivers_details = User.find(user_id)
		@employee_leave = employee_leave
		@unapproved_date = Date.today
		@first_approver_comment = first_approver_comment
		subject = "Leave unapproved by first approver"
		mail(to: @receivers_details.mail, subject: subject)
	end

	def notify_first_approver_for_unapproved_employee_leave(current_user,employee_leave,unapproved_date,first_approver_comment)
		@user = current_user
		@employee_leave = employee_leave
		@unapproved_date = Date.today
		@first_approver_comment = first_approver_comment
		subject = "Unapproved leave of #{@employee_leave.user.name}"
		mail(to: @user.mail, subject: subject)
	end

	# def notify_user_for_cancelled_employee_leave(user_id, employee_leave)
	# 	@receivers_details = User.find(user_id)
	# 	@employee_leave = employee_leave
	# 	subject = "#{@receivers_details.name.titleize} Cancelled the Leave"
	# 	mail(to: @receivers_details.mail, subject: subject)
	# end

	def notify_tl_of_previous_leave(receivers_emails, leave, request_changed_by_user, current_user)
		@leave_details, @changed_status, @user = leave, request_changed_by_user, current_user
		email_to = receivers_emails.join(',')
		subject = "#{@leave_details.systango_hrm_subject.subject} applied by #{@leave_details.applied_user.name.titleize rescue nil} from- #{@leave_details.leave_start_date.strftime("%b %d, %Y")} to- #{@leave_details.leave_end_date.strftime("%b %d, %Y")}"
		mail(to: email_to, subject: subject)
	end

	def notify_all_of_leave_cancelled(leave, current_user, receivers_emails)
		@leave_application, @current_user = leave, current_user
		email_to = receivers_emails.join(',')
		subject = "Leave Cancelled"
		mail(to: email_to, subject: subject)
	end

	def notify_all_of_special_leave_cancelled(leave, current_user, receivers_emails)
		@leave_application, @current_user = leave, current_user
		email_to = receivers_emails.join(',')
		@cancelled_date = Date.today
		subject = "#{@leave_application.applied_user.name.titleize rescue nil} cancelled the Special Request."
		mail(to: email_to, subject: subject)
	end

	def notify_user_of_compoff_leave_cancelled(leave, current_user, receivers_emails, canceled_date)
		@leave_application, @current_user = leave, current_user
		@canceled_date = Date.today
		email_to = receivers_emails.join(',')
		subject = "#{@leave_application.user.name} cancelled the Comp-off Leave."
		mail(to: email_to, subject: subject)
	end

	def notify_all_of_final_approval(leave, request_changed_by_user, current_user, new_status, receivers_emails)
		@leave_application , @changed_status, @current_user, @new_status = leave, request_changed_by_user, current_user, new_status
		email_to = receivers_emails.join(',')
		subject = "#{@leave_application.systango_hrm_subject.subject} applied by #{@leave_application.applied_user.name.titleize rescue nil} from- #{@leave_application.leave_start_date.strftime("%b %d, %Y")} to- #{@leave_application.leave_end_date.strftime("%b %d, %Y")}"
		mail(to: email_to, subject: subject)
	end

end
