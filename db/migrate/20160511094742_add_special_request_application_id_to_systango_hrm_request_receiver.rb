class AddSpecialRequestApplicationIdToSystangoHrmRequestReceiver < ActiveRecord::Migration
  def change
    add_column :systango_hrm_request_receivers, :special_request_application_id, :string
  end
end
