class AddIsStartDateEnabledAndIsEndDateEnabledToSystangoHrmEmployeeLeaves < ActiveRecord::Migration
  def change
    add_column :systango_hrm_employee_leaves, :is_start_date_enabled, :boolean, :default => false
    add_column :systango_hrm_employee_leaves, :is_end_date_enabled, :boolean, :default => false
  end
end
