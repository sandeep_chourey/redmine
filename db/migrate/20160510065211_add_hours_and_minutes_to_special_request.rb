class AddHoursAndMinutesToSpecialRequest < ActiveRecord::Migration
  def change
    # add_column :systango_hrm_special_requests, :hours, :time
    add_column :systango_hrm_special_requests, :hours_minutes, :time
  end
end
