class AddColumnIsDeletedToSystangoHrmSubjects < ActiveRecord::Migration
  def change
    add_column :systango_hrm_subjects, :is_deleted, :boolean, default: true
  end
end
