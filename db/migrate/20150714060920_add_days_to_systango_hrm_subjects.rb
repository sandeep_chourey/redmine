class AddDaysToSystangoHrmSubjects < ActiveRecord::Migration
  def change
    add_column :systango_hrm_subjects, :days, :integer
  end
end
