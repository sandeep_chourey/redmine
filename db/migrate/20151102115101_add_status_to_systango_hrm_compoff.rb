class AddStatusToSystangoHrmCompoff < ActiveRecord::Migration
  def change
    add_column :systango_hrm_compoffs, :status, :string
  end
end
