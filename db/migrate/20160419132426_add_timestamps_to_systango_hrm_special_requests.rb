class AddTimestampsToSystangoHrmSpecialRequests < ActiveRecord::Migration
  def change
    add_column :systango_hrm_special_requests, :created_at, :datetime
  end
end
