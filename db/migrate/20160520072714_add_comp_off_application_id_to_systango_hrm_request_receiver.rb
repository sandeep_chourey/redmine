class AddCompOffApplicationIdToSystangoHrmRequestReceiver < ActiveRecord::Migration
  def change
    add_column :systango_hrm_request_receivers, :comp_off_application_id, :integer
  end
end
