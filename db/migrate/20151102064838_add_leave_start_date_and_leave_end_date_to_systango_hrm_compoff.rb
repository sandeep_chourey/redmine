class AddLeaveStartDateAndLeaveEndDateToSystangoHrmCompoff < ActiveRecord::Migration
  def change
    add_column :systango_hrm_compoffs, :leave_start_date, :datetime
    add_column :systango_hrm_compoffs, :leave_end_date, :datetime
  end
end
