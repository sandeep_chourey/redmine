class AddDateOfJoiningToUsers < ActiveRecord::Migration
  def change
    add_column :users, :date_of_joining, :date
  end
end
