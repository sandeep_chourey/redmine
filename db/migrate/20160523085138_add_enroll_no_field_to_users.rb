class AddEnrollNoFieldToUsers < ActiveRecord::Migration
  def change
    add_column :users, :enroll_no, :integer, :limit => 8
  end
end
