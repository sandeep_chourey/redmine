/**
 * Created by jesal on 2/11/15.
 */
$(document).ready(function(){
    $(function() {
        $( "#systango_hrm_compoff_leave_start_date" ).datepicker({
            dateFormat: "yy-mm-dd",
            changeMonth: true,
            changeYear: true,
            currentText: "Current",
            showOn: 'button',
            buttonImage: '/redmine/images/calendar.png',
            buttonImageOnly: true
        });
    });
});

$(document).ready(function(){
    $(function() {
        $( "#systango_hrm_compoff_leave_end_date" ).datepicker({
            dateFormat: "yy-mm-dd",
            changeMonth: true,
            changeYear: true,
            currentText: "Current",
            showOn: 'button',
            buttonImage: '/redmine/images/calendar.png',
            buttonImageOnly: true
        });
    });
});
